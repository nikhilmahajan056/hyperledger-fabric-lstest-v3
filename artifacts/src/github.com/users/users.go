package main

import (
	"fmt"
	"encoding/json"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserContract struct {
	contractapi.Contract
}

func (uc *UserContract) InitLedger(ctx contractapi.TransactionContextInterface) error {
	users := []User{
		{Username: "user1", Password: "password1"},
		{Username: "user2", Password: "password2"},
	}

	for _, user := range users {
		userJSON, err := json.Marshal(user)
		if err != nil {
			return err
		}

		err = ctx.GetStub().PutState(user.Username, userJSON)
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *UserContract) CreateUser(ctx contractapi.TransactionContextInterface, username, password string) error {
	existingUser, err := ctx.GetStub().GetState(username)
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
	}

	if existingUser != nil {
		return fmt.Errorf("user %s already exists", username)
	}

	user := User{
		Username: username,
		Password: password,
	}

	userJSON, err := json.Marshal(user)
	if err != nil {
		return err
	}

	return ctx.GetStub().PutState(username, userJSON)
}

func (uc *UserContract) AuthenticateUser(ctx contractapi.TransactionContextInterface, username, password string) (bool, error) {
	userJSON, err := ctx.GetStub().GetState(username)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	if userJSON == nil {
		return false, nil
	}

	var user User
	err = json.Unmarshal(userJSON, &user)
	if err != nil {
		return false, err
	}

	return user.Password == password, nil
}

func main() {
	userContract := new(UserContract)

	cc, err := contractapi.NewChaincode(userContract)
	if err != nil {
		fmt.Printf("Error creating chaincode: %v", err)
		return
	}

	if err := cc.Start(); err != nil {
		fmt.Printf("Error starting chaincode: %v", err)
	}
}
