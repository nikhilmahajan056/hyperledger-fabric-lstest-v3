upgradeOrderer() {

    export ORDERER_CONTAINER=orderer.example.com

    mkdir -p /home/ubuntu/Documents/Hyperledger_Fabric/HF_Tests/FabricNetwork-2.x-backup/artifacts/data_backup

    export LEDGERS_BACKUP=/home/ubuntu/Documents/Hyperledger_Fabric/HF_Tests/FabricNetwork-2.x-backup/artifacts/data_backup

    docker stop $ORDERER_CONTAINER

    docker cp $ORDERER_CONTAINER:/var/hyperledger/production/orderer/ $LEDGERS_BACKUP/$ORDERER_CONTAINER

    docker rm -f $ORDERER_CONTAINER

    # docker run -d -v ${PWD}/../data/orderer/var:/var/hyperledger -v ${PWD}/../channel/genesis.block:/var/hyperledger/orderer/genesis.block -v ${PWD}/../channel/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/msp:/var/hyperledger/orderer/msp -v ${PWD}/../channel/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls:/var/hyperledger/orderer/tls --env-file ./envorderer.list --name $ORDERER_CONTAINER --network=artifacts_test -p 7050:7050 -p 8443:8443 -w /opt/gopath/src/github.com/hyperledger/fabric/orderers hyperledger/fabric-orderer:latest orderer

    docker run -d -v ${PWD}/../channel/genesis.block:/var/hyperledger/orderer/genesis.block -v ${PWD}/../channel/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/msp:/var/hyperledger/orderer/msp -v ${PWD}/../channel/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls:/var/hyperledger/orderer/tls --env-file ./envorderer.list --name $ORDERER_CONTAINER --network=artifacts_test -p 7050:7050 -p 8443:8443 hyperledger/fabric-orderer:latest orderer

}

upgradePeer0Org1() {

    export PEER_CONTAINER=peer0.org1.example.com
    
    docker stop $PEER_CONTAINER

    CC_CONTAINERS=$(docker ps | grep dev-$PEER_CONTAINER | awk '{print $1}')
    if [ -n "$CC_CONTAINERS" ] ; then docker rm -f $CC_CONTAINERS ; fi

    CC_IMAGES=$(docker images | grep dev-$PEER | awk '{print $1}')
    if [ -n "$CC_IMAGES" ] ; then docker rmi -f $CC_IMAGES ; fi

    docker rm -f $PEER_CONTAINER

    docker run -d -v /var/run/:/host/var/run/ -v ${PWD}/../channel/config/:/var/hyperledger/fabric/config/ -v ${PWD}/../channel/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp:/etc/hyperledger/crypto/peer/msp -v ${PWD}/../channel/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls:/etc/hyperledger/crypto/peer/tls -v ${PWD}/../data/peer0-org1/var/production/:/var/hyperledger/production/ -v ${PWD}/../channel/:/etc/hyperledger/channel/ --env-file ./envpeer0org1.list --name $PEER_CONTAINER --network=artifacts_test -p 7051:7051 -p 9440:9440 hyperledger/fabric-peer:latest peer node start

}

upgradePeer1Org1() {

    export PEER_CONTAINER=peer1.org1.example.com
    
    docker stop $PEER_CONTAINER

    CC_CONTAINERS=$(docker ps | grep dev-$PEER_CONTAINER | awk '{print $1}')
    if [ -n "$CC_CONTAINERS" ] ; then docker rm -f $CC_CONTAINERS ; fi

    CC_IMAGES=$(docker images | grep dev-$PEER | awk '{print $1}')
    if [ -n "$CC_IMAGES" ] ; then docker rmi -f $CC_IMAGES ; fi

    docker rm -f $PEER_CONTAINER

    # docker run -d -v ${PWD}/data/orderer/var:/var/hyperledger \ 
    # -v ${PWD}/../channel/crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/msp:/etc/hyperledger/crypto/peer/msp \
    # -v ${PWD}/../channel/crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls:/etc/hyperledger/crypto/peer/tls \
    # -v ${PWD}/../var/run/:/host/var/run/ \
    # -v ${PWD}/../data/peer1-org1/var:/var/hyperledger\
    # -v ${PWD}/../channel/:/etc/hyperledger/channel/ \
    # --env-file ./envpeer1org1.list \
    # --name $PEER_CONTAINER \
    # --network=artifacts_test \
    # -p 8051:8051 -p 9441:9441 \
    # -w /opt/gopath/src/github.com/hyperledger/fabric/peer \
    # hyperledger/fabric-peer:latest peer node start

    docker run -d -v /var/run/:/host/var/run/ -v ${PWD}/../channel/config/:/var/hyperledger/fabric/config/ -v ${PWD}/../channel/crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/msp:/etc/hyperledger/crypto/peer/msp -v ${PWD}/../channel/crypto-config/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls:/etc/hyperledger/crypto/peer/tls -v ${PWD}/../data/peer1-org1/var/production/:/var/hyperledger/production/ -v ${PWD}/../channel/:/etc/hyperledger/channel/ --env-file ./envpeer1org1.list --name $PEER_CONTAINER --network=artifacts_test -p 8051:8051 -p 9441:9441 hyperledger/fabric-peer:latest peer node start

}

# upgradeOrderer
# upgradePeer0Org1
upgradePeer1Org1