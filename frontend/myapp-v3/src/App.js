import React from 'react';
import { Routes, Route} from 'react-router-dom';
import Homepage from './components/Homepage';
import Login from './components/Login';
import Signup from './components/Signup'; 
import Dashboard from './components/Dashboard';

function App() {
  return (
      <Routes>
        <Route exact path="/" element={<Homepage />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/login" element={<Login />} />
        <Route path="/dashboard" element={<Dashboard />} />
        {/* <Route path="/update-profile" component={UpdateProfile} />
        <Route path="/employee" exact component={EmployeeList} />
        <Route path="/employee/:id" component={EmployeeDetails} /> */}
      </Routes>
  );
}

export default App;
