import React from 'react';
import logo from '../logo.svg';
import HomepageImage from '../homepageImage.png';
import '../styles/Homepage.css';
import { Link } from 'react-router-dom';

// Home component
const Homepage = () => {
  return (
    <div className="homepage">
      <div className="header">
        <a className="signup-a" href='/'>
          <img className="logo" src={logo} alt="Portal Logo" />
        </a>
        {/* <h1>Employee Management Web Application</h1>           */}
        <div className="header-buttons">
          <Link to="/signup" className="button">Sign Up</Link>
          <Link to="/login" className="button">Login</Link>
        </div>
      </div>
      <div className="homepageContent">
        {/* <h1>Welcome to the Employee Management Portal</h1> */}
        <div className="container">
          <img className="banner-image" src={HomepageImage} alt="Employee Management" />
          <div>
            <h1 className="homepageHeader">Introducing Our Outsourced Employee Management Web Application</h1>
            <p className="homepagePara">Efficiently manage your outsourced workforce with our state-of-the-art employee management portal.</p>
            <p className="homepagePara">Our platform provides a secure and transparent solution for tracking which organization each employee is outsourced to, ensuring seamless coordination and accountability.</p>
            <p className="homepagePara">With advanced privacy measures in place, your employees' project details and daily work remain confidential, guaranteeing confidentiality and maintaining trust between organizations. Streamline your outsourcing processes and enhance productivity with our user-friendly and reliable outsourced employee management system.</p>
          </div>
          
        </div>
      </div>
      <footer className="footer">
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
};

  
export default Homepage;