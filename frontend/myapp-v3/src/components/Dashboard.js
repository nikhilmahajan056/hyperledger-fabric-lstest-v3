import React, { useEffect, useState } from 'react';
import logo from '../logo.svg';
import '../styles/Dashboard.css';

// Dashboard component
const Dashboard = () => {

  const [logs, setLogs] = useState([]);

  const [selectedOrg, setSelectedOrg] = useState('Org1');
  const [fullname, setFullname] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [selectedEmployee, setSelectedEmployee] = useState(null);
  const [addEmployeeClicked, setAddEmployeeClicked] = useState(false);
  const [moveEmployeeClicked, setMoveEmployeeClicked] = useState(false);

  useEffect(() => {
    fetchLogsList();
  }, []);

  const fetchLogsList = async () => {
    try {
      console.log("invoked fetchLogsList");
      const response = await fetch(`http://localhost:4000/getAllLogs/${selectedOrg}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        // body: JSON.stringify({ orgName: selectedOrg }),
      });
      const data = await response.json();
      console.log(data);
      setLogs(data.logs);
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const handleSignup = async () => {
    try {
      console.log("invoked handleSignup", fullname, username, password);
      const response = await fetch('http://localhost:4000/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ fullname, username, password, selectedOrg }),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleRecordClick = (employee) => {
    setSelectedEmployee(employee);
  };

  // const handleSaveEmployee = (updatedEmployee) => {
  //   setEmployees((prevEmployees) =>
  //     prevEmployees.map((employee) =>
  //       employee.employeeID === updatedEmployee.employeeID ? updatedEmployee : employee
  //     )
  //   );
  // };

  const handleClosePopup = () => {
    setSelectedEmployee(null);
    setAddEmployeeClicked(false);
    setMoveEmployeeClicked(false);
  };

  // Employee Popup component
const EditEmployeePopup = ({ employee, onSave, onClose }) => {
  const [firstName, setFirstName] = useState(employee.firstName);
  const [lastName, setLastName] = useState(employee.lastName);
  const [contactNumber, setContactNumber] = useState(employee.contactNumber);
  const [emailID, setEmailID] = useState(employee.emailID);
  const [yearsOfExperience, setYearsOfExperience] = useState(employee.yearsOfExperience);
  const [technologies, setTechnologies] = useState(employee.technologies);
  const [address, setAddress] = useState(employee.address);

  const updateEmployee = async (updatedEmployee) => {
    try {
      console.log("invoked updateEmployee", updatedEmployee);
      const response = await fetch('http://localhost:4000/updateEmployee', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedEmployee),
      });
      const data = await response.json();
      console.log(data);
      return data;
    } catch (error) {
      console.error('Error:', error);
      return
    }
  }

  const handleSave = async () => {
    const updatedEmployee = {
      ...employee,
      firstName,
      lastName,
      contactNumber,
      emailID,
      yearsOfExperience,
      technologies,
      address,
    };

    try {
      const response = await updateEmployee(updatedEmployee);
      if (response.message) {
        onSave(updatedEmployee);
        onClose();
      }
    } catch (error) {
      console.error('Error:', error);        
    }
  };

  return (
    <div className="popup">
      <div className="popup-content">
        <h2>Edit Employee</h2>
        <form>
          <label htmlFor="firstName">First name:</label>
          <input
            type="text"
            id="firstName"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />

          <label htmlFor="lastName">Last name:</label>
          <input
            type="text"
            id="lastName"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />

          <label htmlFor="contactNumber">Contact Number:</label>
          <input
            type="text"
            id="contactNumber"
            value={contactNumber}
            onChange={(e) => setContactNumber(e.target.value)}
          />

          <label htmlFor="emailID">Email:</label>
          <input
            type="text"
            id="emailID"
            value={emailID}
            onChange={(e) => setEmailID(e.target.value)}
          />

          <label htmlFor="yearsOfExperience">Total experience:</label>
          <input
            type="text"
            id="yearsOfExperience"
            value={yearsOfExperience}
            onChange={(e) => setYearsOfExperience(e.target.value)}
          />

          <label htmlFor="tec">Technologies:</label>
          <input
            type="text"
            id="technologies"
            value={technologies}
            onChange={(e) => setTechnologies(e.target.value)}
          />

          <label htmlFor="address">Address:</label>
          <input
            type="text"
            id="address"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />

        </form>
        <div className="popup-buttons">
          <button className="dashboard-button" onClick={handleSave}>Save</button>
          <button className="dashboard-button" onClick={onClose}>Cancel</button>
        </div>
      </div>
    </div>
  );
};

// const AddEmployeePopup = ({ onClose }) => {
//   const [firstName, setFirstName] = useState("");
//   const [lastName, setLastName] = useState("");
//   const [contactNumber, setContactNumber] = useState("");
//   const [emailID, setEmailID] = useState("");
//   const [yearsOfExperience, setYearsOfExperience] = useState("");
//   const [technologies, setTechnologies] = useState("");
//   const [address, setAddress] = useState("");

//   const addEmployee = async (newEmployee) => {
//     try {
//       console.log("invoked updateEmployee", newEmployee);
//       const response = await fetch('http://localhost:4000/createEmployee', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify(newEmployee),
//       });
//       const data = await response.json();
//       console.log(data);
//       return data;
//     } catch (error) {
//       console.error('Error:', error);
//       return
//     }
//   }

//   const handleSave = async () => {
//     const newEmployee = {
//       firstName,
//       lastName,
//       contactNumber,
//       emailID,
//       yearsOfExperience,
//       technologies,
//       address,
//       isAvailable: "true",
//     };

//     try {
//       const response = await addEmployee(newEmployee);
//       if (response.message) {
//         fetchEmployeeList();
//         onClose();
//       }
//     } catch (error) {
//       console.error('Error:', error);        
//     }
//   };

//   return (
//     <div className="popup">
//       <div className="popup-content">
//         <h2>Add new Employee</h2>
//         <form>
//           <label htmlFor="firstName">First name:</label>
//           <input
//             type="text"
//             id="firstName"
//             value={firstName}
//             onChange={(e) => setFirstName(e.target.value)}
//           />

//           <label htmlFor="lastName">Last name:</label>
//           <input
//             type="text"
//             id="lastName"
//             value={lastName}
//             onChange={(e) => setLastName(e.target.value)}
//           />

//           <label htmlFor="contactNumber">Contact Number:</label>
//           <input
//             type="text"
//             id="contactNumber"
//             value={contactNumber}
//             onChange={(e) => setContactNumber(e.target.value)}
//           />

//           <label htmlFor="emailID">Email:</label>
//           <input
//             type="text"
//             id="emailID"
//             value={emailID}
//             onChange={(e) => setEmailID(e.target.value)}
//           />

//           <label htmlFor="yearsOfExperience">Total experience:</label>
//           <input
//             type="text"
//             id="yearsOfExperience"
//             value={yearsOfExperience}
//             onChange={(e) => setYearsOfExperience(e.target.value)}
//           />

//           <label htmlFor="tec">Technologies:</label>
//           <input
//             type="text"
//             id="technologies"
//             value={technologies}
//             onChange={(e) => setTechnologies(e.target.value)}
//           />

//           <label htmlFor="address">Address:</label>
//           <input
//             type="text"
//             id="address"
//             value={address}
//             onChange={(e) => setAddress(e.target.value)}
//           />

//         </form>
//         <div className="popup-buttons">
//           <button className="dashboard-button" onClick={handleSave}>Save</button>
//           <button className="dashboard-button" onClick={onClose}>Cancel</button>
//         </div>
//       </div>
//     </div>
//   );
// };

  const MoveEmployeePopup = ({ employee, onSave, onClose }) => {
    const [projectDetails, setProjectDetails] = useState("");
    const [reportingManager, setReportingManager] = useState("");
    const [salaryDetails, setSalaryDetails] = useState("");
    const [dateOfJoining, setDateOfJoining] = useState("");
  
    useEffect(() => {
      fetchEmployeePrivateData();
    }, [])

    const fetchEmployeePrivateData = async () => {
      try {
        console.log("invoked fetchEmployeePrivateData");
        const response = await fetch(`http://localhost:4000/getPrivateEmployeeData/Org2/${employee.employeeID}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
          // body: JSON.stringify({ orgName: selectedOrg }),
        });
        const data = await response.json();
        console.log(data.result);
        const result = data.result;
        setProjectDetails(result.projectDetails);
        setDateOfJoining(result.dateOfJoining);
        setReportingManager(result.ReportingManager);
        setSalaryDetails(result.salaryDetails);
        // setEmployees(data.employees);
      } catch (error) {
        console.error('Error:', error);
      }
    }

    const moveEmployee = async (movedEmployee) => {
      try {
        console.log("invoked moveEmployee", movedEmployee);
        const response = await fetch('http://localhost:4000/moveEmployee', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(movedEmployee),
        });
        const data = await response.json();
        console.log(data);
        return data;
      } catch (error) {
        console.error('Error:', error);
        return
      }
    }
  
    const handleSave = async () => {
      console.log("dateOfJoining", dateOfJoining);
      const movedEmployee = {
        ...employee,
        projectDetails,
        reportingManager,
        salaryDetails,
        dateOfJoining,
        isAvailable: false,
        orgName: 'Org2',
      };
  
      try {
        const response = await moveEmployee(movedEmployee);
        if (response.message) {
          onSave(movedEmployee);
          onClose();
        }
      } catch (error) {
        console.error('Error:', error);        
      }
    };
  
    return (
      <div className="popup">
        <div className="popup-content">
          <h2>Move Employee</h2>
          <form>
            <label htmlFor="firstName">First name:</label>
            <input
              type="text"
              id="firstName"
              value={employee.firstName}
              disabled
            />
  
            <label htmlFor="lastName">Last name:</label>
            <input
              type="text"
              id="lastName"
              value={employee.lastName}
              disabled
            />
  
            <label htmlFor="projectDetails">Project Details:</label>
            <input
              type="text"
              id="projectDetails"
              value={projectDetails}
              onChange={(e) => setProjectDetails(e.target.value)}
            />
  
            <label htmlFor="reportingManager">Reporting Manager:</label>
            <input
              type="text"
              id="reportingManager"
              value={reportingManager}
              onChange={(e) => setReportingManager(e.target.value)}
            />
  
            <label htmlFor="salaryDetails">Salary Details:</label>
            <input
              type="text"
              id="salaryDetails"
              value={salaryDetails}
              onChange={(e) => setSalaryDetails(e.target.value)}
            />
  
            <label htmlFor="tec">Date of Joining:</label>
            <input
              type="date"
              id="dateOfJoining"
              value={dateOfJoining}
              onChange={(e) => setDateOfJoining(e.target.value)}
            />
  
          </form>
          <div className="popup-buttons">
            <button className="dashboard-button" onClick={handleSave}>Save</button>
            <button className="dashboard-button" onClick={onClose}>Cancel</button>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="dashboard">
      <div className="dashboard-header">
        <a href='/'>
          <img className="dashboard-logo" src={logo} alt="Portal Logo" />
        </a>
        <div className="dashboard-header-buttons">
          {/* <Link to="/signup" className="button">Sign Up</Link> */}
          {/* <Link to="/login" className="button">Login</Link> */}
          <button className="dashboard-button">Logout</button>
        </div>
      </div>
      <div className="dashboard-section">
        <nav class="dashboard-sidebar">
          <a class="dashboard-bar-item-selected" href="/dashboard">Logs</a>
          <a class="dashboard-bar-item" href="#">Link</a>
          <a class="dashboard-bar-item" href="#">Link</a>
          <a class="dashboard-bar-item" href="#">Link</a>
          <a class="dashboard-bar-item" href="#">Link</a>
        </nav>
        <div className="dashboard-section-main">
          <div className="dashboard-content">
            <div className="dashboard-section-header">
              <h1 className="dashboard-content-header">Logs</h1>
              {/* <div className="dashboard-header-buttons"> */}
                {/* <Link to="/signup" className="button">Sign Up</Link> */}
                {/* <Link to="/login" className="button">Login</Link> */}
                {/* <button className="dashboard-section-button" onClick={(e) => {e.preventDefault(); setAddEmployeeClicked(true);}}><b>+</b> Add new employee</button>
              </div> */}
            </div>
            <div className="dashboard-formDiv">
              <table className="customers">
                <tr>
                  <th>Timestamp</th>
                  <th>Data</th>
                </tr>
                {
                  logs && logs.map((log, index) => {
                    console.log(log.data["ietf-restconf:notification"].eventTime)
                    return (
                      log.data["ietf-restconf:notification"].eventTime && <tr key={index}>
                        <td>{log.data["ietf-restconf:notification"].eventTime}</td>
                        <td>{JSON.stringify(log)}</td>
                        {/* <td>{employee.lastName}</td>
                        <td>{employee.contactNumber}</td>
                        <td>{employee.emailID}</td>
                        <td>{employee.yearsOfExperience}</td>
                        <td>{employee.technologies}</td>
                        <td>{employee.address}</td>
                        <td>{employee.isAvailable ? "Yes" : "No"}</td>
                        <td>
                          <button className="dashboard-section-button-edit" onClick={() => handleRecordClick(employee)}>Edit</button>
                          <button className="dashboard-section-button-edit" onClick={(e) => {e.preventDefault(); setMoveEmployeeClicked(true); handleRecordClick(employee)}}>Move</button>
                        </td> */}
                      </tr>
                    )
                  })
                }
              </table>
              {/* {selectedEmployee && !moveEmployeeClicked && (
                <EditEmployeePopup
                  employee={selectedEmployee}
                  onSave={handleSaveEmployee}
                  onClose={handleClosePopup}
                />
              )}
              {addEmployeeClicked && (
                <AddEmployeePopup
                  onClose={handleClosePopup}
                />
              )}
              {moveEmployeeClicked && (
                <MoveEmployeePopup
                  employee={selectedEmployee}
                  onSave={handleSaveEmployee}
                  onClose={handleClosePopup}
                />
              )} */}
              {/* <input
              type="text"
              name="fullname"
              placeholder="Full Name"
              className="dashboard-input"
              onChange={(e) => setFullname(e.target.value)}
              required
            />
            <input
              type="text"
              name="email"
              placeholder="Email"
              className="dashboard-input"
              onChange={(e) => setUsername(e.target.value)}
              required
            />
            <input
              type="password"
              name="password"
              placeholder="Password"
              className="dashboard-input"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            <select
              value={selectedOrg}
              onChange={(e) => setSelectedOrg(e.target.value)}
              className="signupSelect"
            >
              <option value="">Select Organization</option>
              <option value="Org1">Organization 1</option>
              <option value="Org2">Organization 2</option>
            </select>
            <button className="signupButton" type="submit" onClick={handleSignup}>
              Signup
            </button>
            <p>Already have an account? <a href="/login">Login</a></p> */}
            </div>
          </div>
        </div>
      </div>
      <footer className="dashboard-footer">
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
};

export default Dashboard;