/** @type {import('next').NextConfig} */
const nextConfig = {
    serverRuntimeConfig: {
        // Will only be available on the server side
        connectionOrg1: {
            "name": "first-network-org1",
            "version": "1.0.0",
            "client": {
                "organization": "Org1",
                "connection": {
                    "timeout": {
                        "peer": {
                            "endorser": "300"
                        }
                    }
                }
            },
            "organizations": {
                "Org1": {
                    "mspid": "Org1MSP",
                    "peers": [
                        "peer0.org1.example.com"
                    ],
                    "certificateAuthorities": [
                        "ca.org1.example.com"
                    ]
                }
            },
            "peers": {
                "peer0.org1.example.com": {
                    "url": "grpcs://localhost:7051",
                    "tlsCACerts": {
                        "pem": "-----BEGIN CERTIFICATE-----\nMIICFjCCAb2gAwIBAgIUZUmviL5OOOmqcITuDJzpIrBSz0kwCgYIKoZIzj0EAwIw\naDELMAkGA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQK\nEwtIeXBlcmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMt\nY2Etc2VydmVyMB4XDTIzMDYyMDA5NDQwMFoXDTM4MDYxNjA5NDQwMFowaDELMAkG\nA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQKEwtIeXBl\ncmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMtY2Etc2Vy\ndmVyMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEV9nfRjyiugM5VmEoq8GreAWD\nTGWaNIvqM2m7vD4E44/9vTs4phCGburkNvg/mqEyfZIoHVZ41z2OhGcLe1ciCKNF\nMEMwDgYDVR0PAQH/BAQDAgEGMBIGA1UdEwEB/wQIMAYBAf8CAQEwHQYDVR0OBBYE\nFM5aKK8cXJuj8dMAFdAx3UFAWADVMAoGCCqGSM49BAMCA0cAMEQCIDxhlx64m4CC\nG3w+UUoByOtrRjcjKC/3D5SX5+XK0HXAAiBcGPpRwwbF+3XWVXHiclAEvWThFTIt\nHuRsMW/V6Fl8ww==\n-----END CERTIFICATE-----\n"
                    },
                    "grpcOptions": {
                        "ssl-target-name-override": "peer0.org1.example.com",
                        "hostnameOverride": "peer0.org1.example.com"
                    }
                }
        
            
            },
            "certificateAuthorities": {
                "ca.org1.example.com": {
                    "url": "https://localhost:7054",
                    "caName": "ca.org1.example.com",
                    "tlsCACerts": {
                        "pem": "-----BEGIN CERTIFICATE-----\nMIICFjCCAb2gAwIBAgIUZUmviL5OOOmqcITuDJzpIrBSz0kwCgYIKoZIzj0EAwIw\naDELMAkGA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQK\nEwtIeXBlcmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMt\nY2Etc2VydmVyMB4XDTIzMDYyMDA5NDQwMFoXDTM4MDYxNjA5NDQwMFowaDELMAkG\nA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQKEwtIeXBl\ncmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMtY2Etc2Vy\ndmVyMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEV9nfRjyiugM5VmEoq8GreAWD\nTGWaNIvqM2m7vD4E44/9vTs4phCGburkNvg/mqEyfZIoHVZ41z2OhGcLe1ciCKNF\nMEMwDgYDVR0PAQH/BAQDAgEGMBIGA1UdEwEB/wQIMAYBAf8CAQEwHQYDVR0OBBYE\nFM5aKK8cXJuj8dMAFdAx3UFAWADVMAoGCCqGSM49BAMCA0cAMEQCIDxhlx64m4CC\nG3w+UUoByOtrRjcjKC/3D5SX5+XK0HXAAiBcGPpRwwbF+3XWVXHiclAEvWThFTIt\nHuRsMW/V6Fl8ww==\n-----END CERTIFICATE-----\n"
                    },
                    "httpOptions": {
                        "verify": false
                    }
                }
            }
        },
        connectionOrg2: {
            "name": "first-network-org2",
            "version": "1.0.0",
            "client": {
                "organization": "Org2",
                "connection": {
                    "timeout": {
                        "peer": {
                            "endorser": "300"
                        }
                    }
                }
            },
            "organizations": {
                "Org2": {
                    "mspid": "Org2MSP",
                    "peers": [
                        "peer0.org2.example.com"
                    ],
                    "certificateAuthorities": [
                        "ca.org2.example.com"
                    ]
                }
            },
            "peers": {
                "peer0.org2.example.com": {
                    "url": "grpcs://localhost:8051",
                    "tlsCACerts": {
                        "pem": "-----BEGIN CERTIFICATE-----\nMIICFzCCAb2gAwIBAgIUX+sqsvnYWQv480IuaF8lyYxfU0IwCgYIKoZIzj0EAwIw\naDELMAkGA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQK\nEwtIeXBlcmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMt\nY2Etc2VydmVyMB4XDTIzMDYyMDA5NDQwMFoXDTM4MDYxNjA5NDQwMFowaDELMAkG\nA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQKEwtIeXBl\ncmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMtY2Etc2Vy\ndmVyMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAExrdH50d2UCA3tpnRoI+GJU+J\nLNzY5TWf6sTfVQb/1BrsKbwTkgBqKk/HFHwn9HgSMkqAJcSdgRh0krTFNUs5WaNF\nMEMwDgYDVR0PAQH/BAQDAgEGMBIGA1UdEwEB/wQIMAYBAf8CAQEwHQYDVR0OBBYE\nFOeA9yXpt5Z0Oar2im5hqZtGfjyFMAoGCCqGSM49BAMCA0gAMEUCIQDvksEZl9E1\nAi285+N/0CZgKKz60wbMqgFGzUEmc6REmgIgMKjEmu4BpbaNuftMyowFrrRFgWaY\nZ9vZkUgnN2n+jBA=\n-----END CERTIFICATE-----\n"
                    },
                    "grpcOptions": {
                        "ssl-target-name-override": "peer0.org2.example.com",
                        "hostnameOverride": "peer0.org2.example.com"
                    }
                }
        
            
            },
            "certificateAuthorities": {
                "ca.org2.example.com": {
                    "url": "https://localhost:8054",
                    "caName": "ca.org2.example.com",
                    "tlsCACerts": {
                        "pem": "-----BEGIN CERTIFICATE-----\nMIICFzCCAb2gAwIBAgIUX+sqsvnYWQv480IuaF8lyYxfU0IwCgYIKoZIzj0EAwIw\naDELMAkGA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQK\nEwtIeXBlcmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMt\nY2Etc2VydmVyMB4XDTIzMDYyMDA5NDQwMFoXDTM4MDYxNjA5NDQwMFowaDELMAkG\nA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMRQwEgYDVQQKEwtIeXBl\ncmxlZGdlcjEPMA0GA1UECxMGRmFicmljMRkwFwYDVQQDExBmYWJyaWMtY2Etc2Vy\ndmVyMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAExrdH50d2UCA3tpnRoI+GJU+J\nLNzY5TWf6sTfVQb/1BrsKbwTkgBqKk/HFHwn9HgSMkqAJcSdgRh0krTFNUs5WaNF\nMEMwDgYDVR0PAQH/BAQDAgEGMBIGA1UdEwEB/wQIMAYBAf8CAQEwHQYDVR0OBBYE\nFOeA9yXpt5Z0Oar2im5hqZtGfjyFMAoGCCqGSM49BAMCA0gAMEUCIQDvksEZl9E1\nAi285+N/0CZgKKz60wbMqgFGzUEmc6REmgIgMKjEmu4BpbaNuftMyowFrrRFgWaY\nZ9vZkUgnN2n+jBA=\n-----END CERTIFICATE-----\n"
                    },
                    "httpOptions": {
                        "verify": false
                    }
                }
            }
        },
    },
    publicRuntimeConfig: {
        paginationSize: 5,
    }
}

module.exports = nextConfig
