'use client'

import Image from 'next/image'
import styles from './page.module.css'
import { useState } from 'react';
import md5 from 'md5';
import validator from 'validator';

export default function Signup() {
  const [selectedOrg, setSelectedOrg] = useState('Org1');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [successMsg, setSuccessMsg] = useState('');

  const handleSignup = async () => {
    try {
      setLoading(true);
      setError('');
      setSuccessMsg('');
      if (!validator.isEmail(username)) {
        throw "Invalid email"
      }
      if (!validator.isLength(password, 8)) {
        throw "Password must be at least 8 characters long"
      }
      if (password !== confirmPassword) {
        throw "Password does not match confirm password"
      }
      // console.log("invoked handleSignup", selectedOrg, username, password, md5(password));
      const response = await fetch('/signup/api', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password: md5(password), orgName: selectedOrg }),
      });
      const data = await response.json();
      if (response.status === 200) {
        // console.log(data);
        // alert(data.message);
        setLoading(false);
        setSuccessMsg(data.message);
      } else {
        throw data.error;
      }
    } catch (error) {
      console.error('Error:', error);
      setError(`Error: ${error}`);
      setLoading(false);
    }
  };

  return (
    <div className={styles.homepage}>
      <div className={styles.header}>
        <a className={styles.signupA} href='/'>
          <Image className={styles.logo} src="/vercel.svg" width={100} height={24} alt="Portal Logo" />
        </a>
        {/* <div className="header-buttons">
          <Link to="/signup" className="button">Sign Up</Link>
          <Link to="/login" className="button">Login</Link>
        </div> */}
      </div>
      <div className={styles.content}>
        <h1>Create a new account</h1>
        <div className={styles.formDiv}>
          <input
            type="text"
            name="email"
            placeholder="Email"
            className={styles.input}
            onChange={(e) => setUsername(e.target.value)}
            required
            disabled={loading}
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            className={styles.input}
            onChange={(e) => setPassword(e.target.value)}
            required
            disabled={loading}
          />
          <input
            type="password"
            name="confirmPassword"
            placeholder="Confirm Password"
            className={styles.input}
            onChange={(e) => setConfirmPassword(e.target.value)}
            required
            disabled={loading}
          />
          <select
            value={selectedOrg}
            onChange={(e) => setSelectedOrg(e.target.value)}
            className={styles.signupSelect}
            disabled={loading}
          >
            {/* <option value="">Select Organization</option> */}
            <option value="Org1">Organization 1</option>
            <option value="Org2">Organization 2</option>
          </select>
          <button className={styles.signupButton} type="submit" onClick={handleSignup} disabled={loading}>
            Signup
          </button>
          <p className={styles.pError}>{error}</p>
          <p className={styles.pSuccess}>{successMsg}</p>
          <p className={styles.p}>Already have an account? <a className={styles.signupA} href="/login">Login</a></p>
        </div>
      </div>
      <footer className={styles.footer}>
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
}
