'use client'

import Image from 'next/image'
import styles from './page.module.css'
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import md5 from 'md5';
import validator from 'validator';

export default function Login() {
  const router = useRouter()

  const [selectedOrg, setSelectedOrg] = useState('Org1');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const handleLogin = async () => {
    if (!loading) {
      setLoading(true);
      setError('');
      try {
        if (!validator.isEmail(username)) {
          throw "Invalid email"
        }
        // console.log("invoked handleLogin", selectedOrg, username, password);
        const response = await fetch('/login/api', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ username, password: md5(password), orgName: selectedOrg }),
        });
        const data = await response.json();
        if (response.status === 200) {
          router.push(`/dashboard?orgName=${selectedOrg}`)
        } else {
          throw data.error;
        }
        console.log(data);
        setLoading(false);
      } catch (error) {
        console.error('Error:', error);
        setError(`Error: ${error}`);
        setLoading(false);
      }
    }
  };

  return (
    <div className={styles.homepage}>
      <div className={styles.header}>
        <a className={styles.signupA} href='/'>
          <Image className={styles.logo} src="/vercel.svg" width={100} height={24} alt="Portal Logo" />
        </a>
        {/* <div className="header-buttons">
          <Link to="/signup" className="button">Sign Up</Link>
          <Link to="/login" className="button">Login</Link>
        </div> */}
      </div>
      <div className={styles.content}>
        <h1>Sign in</h1>
        <div className={styles.formDiv}>
          <input
            type="text"
            name="email"
            placeholder="Email"
            className={styles.input}
            onChange={(e) => setUsername(e.target.value)}
            required
            disabled={loading}
          />
          <input
            type="password"
            name="password"
            placeholder="Password"
            className={styles.input}
            onChange={(e) => setPassword(e.target.value)}
            required
            disabled={loading}
          />
          <select
            value={selectedOrg}
            onChange={(e) => setSelectedOrg(e.target.value)}
            className={styles.loginSelect}
            disabled={loading}
          >
            {/* <option value="">Select Organization</option> */}
            <option value="Org1">Organization 1</option>
            <option value="Org2">Organization 2</option>
          </select>
          <button className={styles.loginButton} type="submit" onClick={handleLogin} disabled={loading}>
            Login
          </button>
          <p className={styles.pError}>{error}</p>
          <p className={styles.p}>Don't have an account? <a href="/signup" className={styles.loginA}>Signup</a></p>
        </div>
      </div>
      <footer className={styles.footer}>
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
}
