'use client'

import Image from 'next/image'
import styles from './page.module.css'
import { useState, useEffect, useRef } from 'react';
import { useSearchParams } from 'next/navigation'
import { useRouter } from 'next/navigation';
import { io } from 'socket.io-client';

export default function Dashboard() {
  const router = useRouter()
  const searchParams = useSearchParams()
  const orgName = searchParams.get('orgName')
  const socket = useRef();

  const [logs, setLogs] = useState([]);
  const [selectedOrg, setSelectedOrg] = useState(orgName);
  const [bookmark, setBookmark] = useState('');
  const [prevBookmark, setPrevBookmark] = useState(null);
  const [nextBookmark, setNextBookmark] = useState('');
  const [disabledNext, setDisabledNext] = useState(false);
  const [allBookmarks, setAllBookmarks] = useState([null]);

  useEffect(() => {
    fetchLogsList(bookmark);
    const event = () => {
      socket.current = io.connect('http://localhost:4000/');
      socket.current.on('log registered', () => { fetchLogsList("");} );
    }
    return event();
  }, []);

  const fetchLogsList = async (bookmark) => {
    try {
      const response = await fetch(`/dashboard/api?orgName=${selectedOrg}&bookmark=${bookmark}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        // body: JSON.stringify({ orgName: selectedOrg }),
      });
      const data = await response.json();
      if (response.status === 401) {
          router.push('/')
      }
      console.log(data);
      setLogs(data.logs.records);
      setNextBookmark(data.logs.bookmark);
      setPrevBookmark(null);
      setAllBookmarks(bookmarks => [...bookmarks,bookmark]);
      data.logs.records && data.logs.records.map((log) => {
        if (log.ID === 1) {
          setDisabledNext(true);
        } else {
          setDisabledNext(false);
        }
      })
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const handleNextClicked = async (bookmark) => {
    try {
      const response = await fetch(`/dashboard/api?orgName=${selectedOrg}&bookmark=${bookmark}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        // body: JSON.stringify({ orgName: selectedOrg }),
      });
      const data = await response.json();
      if (response.status === 401) {
          router.push('/')
      }
      console.log(data);
      setLogs(data.logs.records);
      setNextBookmark(data.logs.bookmark);
      setPrevBookmark(allBookmarks[allBookmarks.length - 1]);
      setAllBookmarks(bookmarks => [...bookmarks,bookmark]);
      data.logs.records && data.logs.records.map((log) => {
        if (log.ID === 1) {
          setDisabledNext(true);
        } else {
          setDisabledNext(false);
        }
      })
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const handleBackClicked = async (bookmark) => {
    try {
      const response = await fetch(`/dashboard/api?orgName=${selectedOrg}&bookmark=${bookmark}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        // body: JSON.stringify({ orgName: selectedOrg }),
      });
      const data = await response.json();
      if (response.status === 401) {
          router.push('/')
      }
      console.log(data);
      setLogs(data.logs.records);
      setNextBookmark(data.logs.bookmark);
      let intBookmarks = allBookmarks;
      intBookmarks.pop();
      setAllBookmarks(intBookmarks);
      setPrevBookmark(allBookmarks[allBookmarks.length - 2]);
      data.logs.records && data.logs.records.map((log) => {
        if (log.ID === 1) {
          setDisabledNext(true);
        } else {
          setDisabledNext(false);
        }
      })
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const logout = async () => {
    try {
      const response = await fetch(`/dashboard/api`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        // body: JSON.stringify({ orgName: selectedOrg }),
      });
      const data = await response.json();
      console.log(data);
      if (response.status === 200) {
        router.push('/')
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }


  return (
    <div className={styles.homepage}>
      <div className={styles.header}>
        <a className={styles.signupA}>
          <Image className={styles.logo} src="/vercel.svg" width={100} height={24} alt="Portal Logo" />
        </a>
        <div className={styles.headerButtons}>
          {/* <Link to="/signup" className="button">Sign Up</Link> */}
          {/* <Link to="/login" className="button">Login</Link> */}
          <a className={styles.button} onClick={logout}>Logout</a>
        </div>
      </div>
      <div className={styles.dashboardSection}>
        <nav className={styles.dashboardSidebar}>
          <a className={styles.dashboardBarItemSelected} href={`/dashboard?orgName=${selectedOrg}`}>Logs</a>
          <a className={styles.dashboardBarItem} href="#">Link</a>
          <a className={styles.dashboardBarItem} href="#">Link</a>
          <a className={styles.dashboardBarItem} href="#">Link</a>
          <a className={styles.dashboardBarItem} href="#">Link</a>
        </nav>
        <div className={styles.dashboardSectionMain}>
          <div className={styles.dashboardContent}>
            <div className={styles.dashboardSectionHeader}>
              <h1 className={styles.dashboardContentHeader}>Logs</h1>
              {/* <div className="dashboard-header-buttons"> */}
                {/* <Link to="/signup" className="button">Sign Up</Link> */}
                {/* <Link to="/login" className="button">Login</Link> */}
                {/* <button className="dashboard-section-button" onClick={(e) => {e.preventDefault(); setAddEmployeeClicked(true);}}><b>+</b> Add new employee</button>
              </div> */}
            </div>
            <div className={styles.dashboardFormDiv}>
              <table className={styles.customers}>
                <tr>
                  <th>Timestamp</th>
                  <th>Data</th>
                </tr>
                {
                  logs && logs.map((log, index) => {
                    // console.log(log.data["ietf-restconf:notification"].eventTime)
                    // if (log.ID === 1) {
                    //   setDisabledNext(true);
                    // }
                    return (
                      log.data["ietf-restconf:notification"].eventTime && <tr key={index}>
                        <td>{log.data["ietf-restconf:notification"].eventTime}</td>
                        <td>{JSON.stringify({"data": log.data})}</td>
                        {/* <td>{employee.lastName}</td>
                        <td>{employee.contactNumber}</td>
                        <td>{employee.emailID}</td>
                        <td>{employee.yearsOfExperience}</td>
                        <td>{employee.technologies}</td>
                        <td>{employee.address}</td>
                        <td>{employee.isAvailable ? "Yes" : "No"}</td>
                        <td>
                          <button className="dashboard-section-button-edit" onClick={() => handleRecordClick(employee)}>Edit</button>
                          <button className="dashboard-section-button-edit" onClick={(e) => {e.preventDefault(); setMoveEmployeeClicked(true); handleRecordClick(employee)}}>Move</button>
                        </td> */}
                      </tr>
                    )
                  })
                }
              </table>
              <div className={styles.paginationButtons}>
                { prevBookmark !== null && <button className={styles.dashboardSectionButton} onClick={(e) => {e.preventDefault(); handleBackClicked(prevBookmark);}}><b>&#8592;</b> Back</button> }
                { !disabledNext && <button className={styles.dashboardSectionButton} onClick={(e) => {e.preventDefault(); handleNextClicked(nextBookmark);}}>Next <b>&#8594;</b></button> }
              </div>
              {/* {selectedEmployee && !moveEmployeeClicked && (
                <EditEmployeePopup
                  employee={selectedEmployee}
                  onSave={handleSaveEmployee}
                  onClose={handleClosePopup}
                />
              )}
              {addEmployeeClicked && (
                <AddEmployeePopup
                  onClose={handleClosePopup}
                />
              )}
              {moveEmployeeClicked && (
                <MoveEmployeePopup
                  employee={selectedEmployee}
                  onSave={handleSaveEmployee}
                  onClose={handleClosePopup}
                />
              )} */}
              {/* <input
              type="text"
              name="fullname"
              placeholder="Full Name"
              className="dashboard-input"
              onChange={(e) => setFullname(e.target.value)}
              required
            />
            <input
              type="text"
              name="email"
              placeholder="Email"
              className="dashboard-input"
              onChange={(e) => setUsername(e.target.value)}
              required
            />
            <input
              type="password"
              name="password"
              placeholder="Password"
              className="dashboard-input"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            <select
              value={selectedOrg}
              onChange={(e) => setSelectedOrg(e.target.value)}
              className="signupSelect"
            >
              <option value="">Select Organization</option>
              <option value="Org1">Organization 1</option>
              <option value="Org2">Organization 2</option>
            </select>
            <button className="signupButton" type="submit" onClick={handleSignup}>
              Signup
            </button>
            <p>Already have an account? <a href="/login">Login</a></p> */}
            </div>
          </div>
        </div>
      </div>
      <footer className={styles.footer}>
        <p>&copy; 2023 YourCompany. All rights reserved.</p>
      </footer>
    </div>
  );
}
