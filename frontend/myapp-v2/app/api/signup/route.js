import { Gateway, Wallets } from 'fabric-network';
import path from 'path';
import fs from 'fs';

// const ccpPath = path.resolve(__dirname, 'connection.json');
// const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
// const ccp = JSON.parse(ccpJSON);

// const channelName = 'mychannel';
// const chaincodeName = 'userchaincode';
// const walletPath = path.join(process.cwd(), 'wallet');
// const wallet = await Wallets.newFileSystemWallet(walletPath);
// const identityLabel = 'admin';
// const gateway = new Gateway();
import { NextResponse } from 'next/server';

export async function POST(request) {
  // const { searchParams } = new URL(request.url);
  // const id = searchParams.get('id');
  // const res = await fetch(`https://data.mongodb-api.com/product/${id}`, {
  //   headers: {
  //     'Content-Type': 'application/json',
  //     'API-Key': process.env.DATA_API_KEY,
  //   },
  // });
  // const product = await res.json();
  const data = await request.json();
  console.log("body is:", data);
  let ccp = await getCCP(data.selectedOrg)
  console.log("ccp is:", ccp);
  return NextResponse.json({ name: 'John Doe' });
}

const getCCP = async (org) => {
  let ccpPath = null;
  const folder = "config/";
  org == 'Org1' ? ccpPath = `${folder}connection-org1.json` : null
  org == 'Org2' ? ccpPath = `${folder}connection-org2.json` : null
  const ccpJSON = fs.readFileSync(ccpPath, 'utf8')
  const ccp = JSON.parse(ccpJSON);
  return ccp
}