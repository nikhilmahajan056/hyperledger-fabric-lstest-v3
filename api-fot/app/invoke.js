const { Gateway, Wallets, TxEventHandler, GatewayOptions, DefaultEventHandlerStrategies, TxEventHandlerFactory } = require('fabric-network');
const EventStrategies = require('fabric-network/lib/impl/event/defaulteventhandlerstrategies');
const path = require("path")
const helper = require('./helper');

let invocation_connection;

const connection= async(username,org_name)=>{
        const ccp = await helper.getCCP(org_name);
        const walletPath = await helper.getWalletPath(org_name);
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        let identity = await wallet.get(username);
        if (!identity) {
            console.log(`An identity for the user ${username} does not exist in the wallet, so registering user`);
            await helper.getRegisteredUser(username, org_name, true)
            identity = await wallet.get(username);
            console.log('Run the registerUser.js application before retrying');
            return;
        }

    const connectOptions = {
        wallet, identity: username, discovery: { enabled: true, asLocalhost: false },
        // eventHandlerOptions: EventStrategies.NONE
        eventHandlerOptions: {
            commitTimeout: 100,
            strategy: DefaultEventHandlerStrategies.NETWORK_SCOPE_ANYFORTX
        }
    }

    invocation_connection = new Gateway();
    await invocation_connection.connect(ccp, connectOptions);
}

const invokeTransaction = async (channelName, chaincodeName, fcn, args, username, org_name) => {
    try {
        const network = await invocation_connection.getNetwork(channelName);
        const contract = await network.getContract(chaincodeName);    


        // Multiple smartcontract in one chaincode
        let result;

        switch (fcn) {
            case "InsertProductRecords":
		        console.log("====== Executing InsertProductRecords ======")
                result = await contract.submitTransaction(fcn, args[0]);
                console.log(`Output: InsertProductRecords= ${result.toString()}`)
                result = {txid: result.toString()}
                // await gateway.disconnect();
                break;
            case "UpdateProduct":
                console.log("====== Executing UpdateProduct ======")
                result = await contract.submitTransaction(fcn, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12]);
                console.log(`Output: UpdateProduct= ${result.toString()}`)
                result = {txid: result.toString()}
                // await gateway.disconnect();
                break;
            case "DeleteProductByuniqueID":
	        	console.log("====== Executing DeleteProductByUniqueId ======")
                result = await contract.submitTransaction(fcn, args[0]);
                console.log(`Output: DeleteProductByuniqueID= ${result.toString()}`)
                result = {txid: result.toString()}
                // await gateway.disconnect();
                break;
	       case "GetProductForQuery":
                console.log("====== Executing GetProductForQuery ======")
                result = await contract.evaluateTransaction(fcn, args[0]);
                console.log(`Output: GetProductForQuery= ${result.toString()}`)
                result = JSON.parse(result.toString());
                // await gateway.disconnect();
                break;

            default:	
                break;
        }
        const response_payload = {
            result: result,
            error: false,
            errorData: null
        }

        console.log("Invoke Transaction Try Block Response Payload: ", response_payload)
        return response_payload;


    } catch (error) {
        console.log(`Invoke Transaction Getting error: ${error}`)
        const response_payload = {
            result: null,
            error: true,
            errorData: error.message
        }
        console.log("Invoke Transaction Catch Block Response Payload: ", response_payload)
        return response_payload

    }
}

exports.invokeTransaction = invokeTransaction;
exports.connection = connection;